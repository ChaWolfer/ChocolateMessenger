<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Message;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Author
 *
 * @ORM\Table(name="author")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IAuthorRepository")
 */
class Author
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="FirstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="Mail", type="string", length=255)
     */
    private $mail;

    /**
     * @var bool
     *
     * @ORM\Column(name="Gender", type="boolean")
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="Photo", type="string", length=255)
     */
    private $photo;


    /**
    * @var ArrayCollection
    *
    * @ORM\OneToMany(targetEntity="Message", mappedBy="author")
    */
    private $messages;


    public function __construc(){
      $this->$messages= new ArrayCollection;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Author
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Author
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Author
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }


    /**
     * Set gender
     *
     * @param bool $gender
     *
     * @return Author
     */
    public function setGender($gender)
    {
        $this->gender = $gener;

        return $this;
    }

    /**
     * Get gender
     *
     * @return bool
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Author
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }


    /**
    * Add Message
    *
    * @param Message $message
    *
    * @return Author
    */
    public function addMessage(Message $message)
    {
      $this->messages[]=$message;
      return $this;
    }

    /**
    * Remove Message
    *
    * @param Message $message
    *
    * @return Author
    */
    public function removeMessage(Message $message)
    {
      $this->messages->removeElement($message);
      return $this;
    }

    /**
    * Get messages
    *
    * @return ArrayCollection
    */
    public function getMessages()
    {
      return $this->messages;
    }

    /**
    * Set messages
    *
    * @param ArrayCollection $messages cette liste contient des objets de type Message
    *
    * @return Author
    */
    public function setMessages(ArrayCollection $messages)
    {
      $this->messages=$messages;
    }

}
