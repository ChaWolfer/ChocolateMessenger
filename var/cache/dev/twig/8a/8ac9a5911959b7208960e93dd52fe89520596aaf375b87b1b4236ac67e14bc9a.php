<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_12f738d54aff3bf3f6a56dc81f2505fdcba5d762d3d028c6a50f1a44ad74da5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c8af6af5f8da5fbd39e8463665bdb80aaebd1901c5b2c6fa330039e2709446d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c8af6af5f8da5fbd39e8463665bdb80aaebd1901c5b2c6fa330039e2709446d->enter($__internal_1c8af6af5f8da5fbd39e8463665bdb80aaebd1901c5b2c6fa330039e2709446d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_4e3bc29637b3fe592721326770fba276d04d346f732af93b175ac9714928ec57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e3bc29637b3fe592721326770fba276d04d346f732af93b175ac9714928ec57->enter($__internal_4e3bc29637b3fe592721326770fba276d04d346f732af93b175ac9714928ec57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1c8af6af5f8da5fbd39e8463665bdb80aaebd1901c5b2c6fa330039e2709446d->leave($__internal_1c8af6af5f8da5fbd39e8463665bdb80aaebd1901c5b2c6fa330039e2709446d_prof);

        
        $__internal_4e3bc29637b3fe592721326770fba276d04d346f732af93b175ac9714928ec57->leave($__internal_4e3bc29637b3fe592721326770fba276d04d346f732af93b175ac9714928ec57_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_677b3cdf00ca20dcb314f713099c7a99177b2669d656f2fa5201aa9a7a40f15c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_677b3cdf00ca20dcb314f713099c7a99177b2669d656f2fa5201aa9a7a40f15c->enter($__internal_677b3cdf00ca20dcb314f713099c7a99177b2669d656f2fa5201aa9a7a40f15c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2cf87402f035497e83bc3313643310fe3f55d1332875fdff4ac41a65b38fc3a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2cf87402f035497e83bc3313643310fe3f55d1332875fdff4ac41a65b38fc3a4->enter($__internal_2cf87402f035497e83bc3313643310fe3f55d1332875fdff4ac41a65b38fc3a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_2cf87402f035497e83bc3313643310fe3f55d1332875fdff4ac41a65b38fc3a4->leave($__internal_2cf87402f035497e83bc3313643310fe3f55d1332875fdff4ac41a65b38fc3a4_prof);

        
        $__internal_677b3cdf00ca20dcb314f713099c7a99177b2669d656f2fa5201aa9a7a40f15c->leave($__internal_677b3cdf00ca20dcb314f713099c7a99177b2669d656f2fa5201aa9a7a40f15c_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a15f429dbee370800ded2797d35e89980f232d1e189eadb741ed3e5447eefb07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a15f429dbee370800ded2797d35e89980f232d1e189eadb741ed3e5447eefb07->enter($__internal_a15f429dbee370800ded2797d35e89980f232d1e189eadb741ed3e5447eefb07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_18540bef049258b39ee8034a48d4a9a7acfda8cfffbed082de237c5a4b7c13ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18540bef049258b39ee8034a48d4a9a7acfda8cfffbed082de237c5a4b7c13ce->enter($__internal_18540bef049258b39ee8034a48d4a9a7acfda8cfffbed082de237c5a4b7c13ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_18540bef049258b39ee8034a48d4a9a7acfda8cfffbed082de237c5a4b7c13ce->leave($__internal_18540bef049258b39ee8034a48d4a9a7acfda8cfffbed082de237c5a4b7c13ce_prof);

        
        $__internal_a15f429dbee370800ded2797d35e89980f232d1e189eadb741ed3e5447eefb07->leave($__internal_a15f429dbee370800ded2797d35e89980f232d1e189eadb741ed3e5447eefb07_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e01d2e3879592804c99f1a936bc1bf910dd8231f047631b0762bc67a11636bc8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e01d2e3879592804c99f1a936bc1bf910dd8231f047631b0762bc67a11636bc8->enter($__internal_e01d2e3879592804c99f1a936bc1bf910dd8231f047631b0762bc67a11636bc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_93d38429e5364117b1fafff3c4bec9bb7a33c509058b86d29e3f64c4bfb50305 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93d38429e5364117b1fafff3c4bec9bb7a33c509058b86d29e3f64c4bfb50305->enter($__internal_93d38429e5364117b1fafff3c4bec9bb7a33c509058b86d29e3f64c4bfb50305_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_93d38429e5364117b1fafff3c4bec9bb7a33c509058b86d29e3f64c4bfb50305->leave($__internal_93d38429e5364117b1fafff3c4bec9bb7a33c509058b86d29e3f64c4bfb50305_prof);

        
        $__internal_e01d2e3879592804c99f1a936bc1bf910dd8231f047631b0762bc67a11636bc8->leave($__internal_e01d2e3879592804c99f1a936bc1bf910dd8231f047631b0762bc67a11636bc8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
