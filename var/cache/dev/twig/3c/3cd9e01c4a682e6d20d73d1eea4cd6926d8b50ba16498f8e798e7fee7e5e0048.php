<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_490088ef8c2d536ba9aa3e4da532e3c9d5d6875dde7bb492995d7d77f4e311f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33726de638c25e6970b38a7794a86256430f4cd12c1e2bea6608347e5fcd4e85 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33726de638c25e6970b38a7794a86256430f4cd12c1e2bea6608347e5fcd4e85->enter($__internal_33726de638c25e6970b38a7794a86256430f4cd12c1e2bea6608347e5fcd4e85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_67b8fc74e45ad253a13761745c1e63f64300bd0acbb2f72cebfd7a95fe3860bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67b8fc74e45ad253a13761745c1e63f64300bd0acbb2f72cebfd7a95fe3860bc->enter($__internal_67b8fc74e45ad253a13761745c1e63f64300bd0acbb2f72cebfd7a95fe3860bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_33726de638c25e6970b38a7794a86256430f4cd12c1e2bea6608347e5fcd4e85->leave($__internal_33726de638c25e6970b38a7794a86256430f4cd12c1e2bea6608347e5fcd4e85_prof);

        
        $__internal_67b8fc74e45ad253a13761745c1e63f64300bd0acbb2f72cebfd7a95fe3860bc->leave($__internal_67b8fc74e45ad253a13761745c1e63f64300bd0acbb2f72cebfd7a95fe3860bc_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_83b2902baf3ff40d5d1c4f3623cb905cd901977d917e1da56f75b2deb66fa5eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83b2902baf3ff40d5d1c4f3623cb905cd901977d917e1da56f75b2deb66fa5eb->enter($__internal_83b2902baf3ff40d5d1c4f3623cb905cd901977d917e1da56f75b2deb66fa5eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_b0bacd1f9fa4255e0eb1c6902cbcacfb7322bc330a2321d56d2a46363e47b252 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0bacd1f9fa4255e0eb1c6902cbcacfb7322bc330a2321d56d2a46363e47b252->enter($__internal_b0bacd1f9fa4255e0eb1c6902cbcacfb7322bc330a2321d56d2a46363e47b252_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b0bacd1f9fa4255e0eb1c6902cbcacfb7322bc330a2321d56d2a46363e47b252->leave($__internal_b0bacd1f9fa4255e0eb1c6902cbcacfb7322bc330a2321d56d2a46363e47b252_prof);

        
        $__internal_83b2902baf3ff40d5d1c4f3623cb905cd901977d917e1da56f75b2deb66fa5eb->leave($__internal_83b2902baf3ff40d5d1c4f3623cb905cd901977d917e1da56f75b2deb66fa5eb_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_734738c4a729bd71692d39db9b6a03046041b3477128481a39e8e0ef4af28fc7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_734738c4a729bd71692d39db9b6a03046041b3477128481a39e8e0ef4af28fc7->enter($__internal_734738c4a729bd71692d39db9b6a03046041b3477128481a39e8e0ef4af28fc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_bc5e6b4d1f9bbcc811cd05320a43d2460ac34578711bcc43431fc648414ee25c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc5e6b4d1f9bbcc811cd05320a43d2460ac34578711bcc43431fc648414ee25c->enter($__internal_bc5e6b4d1f9bbcc811cd05320a43d2460ac34578711bcc43431fc648414ee25c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_bc5e6b4d1f9bbcc811cd05320a43d2460ac34578711bcc43431fc648414ee25c->leave($__internal_bc5e6b4d1f9bbcc811cd05320a43d2460ac34578711bcc43431fc648414ee25c_prof);

        
        $__internal_734738c4a729bd71692d39db9b6a03046041b3477128481a39e8e0ef4af28fc7->leave($__internal_734738c4a729bd71692d39db9b6a03046041b3477128481a39e8e0ef4af28fc7_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_10942acce51b5eaa350f486f86543f42491595a9c857f6a92f0f03a1b9213e29 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10942acce51b5eaa350f486f86543f42491595a9c857f6a92f0f03a1b9213e29->enter($__internal_10942acce51b5eaa350f486f86543f42491595a9c857f6a92f0f03a1b9213e29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_937a28c6d90efd99bc7dd39405f73835bfb17846f30e6e34698838bb23b89255 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_937a28c6d90efd99bc7dd39405f73835bfb17846f30e6e34698838bb23b89255->enter($__internal_937a28c6d90efd99bc7dd39405f73835bfb17846f30e6e34698838bb23b89255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_937a28c6d90efd99bc7dd39405f73835bfb17846f30e6e34698838bb23b89255->leave($__internal_937a28c6d90efd99bc7dd39405f73835bfb17846f30e6e34698838bb23b89255_prof);

        
        $__internal_10942acce51b5eaa350f486f86543f42491595a9c857f6a92f0f03a1b9213e29->leave($__internal_10942acce51b5eaa350f486f86543f42491595a9c857f6a92f0f03a1b9213e29_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
