<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_05e4ba2fd294fdec1729ead3147bccf54b5ac9a204e9543a8a9ab09d10e5ee8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1bf65543713d42d9a4fc1e6f487c17c5f7d97af165e7e16ee0536804358942d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bf65543713d42d9a4fc1e6f487c17c5f7d97af165e7e16ee0536804358942d2->enter($__internal_1bf65543713d42d9a4fc1e6f487c17c5f7d97af165e7e16ee0536804358942d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_0d98b575eded368d6adc69ad70ec75c136c13eb74c7d8570e244134120351249 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d98b575eded368d6adc69ad70ec75c136c13eb74c7d8570e244134120351249->enter($__internal_0d98b575eded368d6adc69ad70ec75c136c13eb74c7d8570e244134120351249_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_1bf65543713d42d9a4fc1e6f487c17c5f7d97af165e7e16ee0536804358942d2->leave($__internal_1bf65543713d42d9a4fc1e6f487c17c5f7d97af165e7e16ee0536804358942d2_prof);

        
        $__internal_0d98b575eded368d6adc69ad70ec75c136c13eb74c7d8570e244134120351249->leave($__internal_0d98b575eded368d6adc69ad70ec75c136c13eb74c7d8570e244134120351249_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
