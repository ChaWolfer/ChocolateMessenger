<?php

/* base.html.twig */
class __TwigTemplate_d81bae7d5325a7f23ba92e849144467ab8ed2421c201cbf1d70c17582ad76de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_546a44ee14b5ad94872ae91146aef7f2e66eb367f1b088506bbd3eacc8db5bc2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_546a44ee14b5ad94872ae91146aef7f2e66eb367f1b088506bbd3eacc8db5bc2->enter($__internal_546a44ee14b5ad94872ae91146aef7f2e66eb367f1b088506bbd3eacc8db5bc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_c8af7767a1ddbfd97a6229827c321608a0777422369bd00c4e7e668d69f6abfd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8af7767a1ddbfd97a6229827c321608a0777422369bd00c4e7e668d69f6abfd->enter($__internal_c8af7767a1ddbfd97a6229827c321608a0777422369bd00c4e7e668d69f6abfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>

        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
        <!-- datetimepicker -->
        <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\"/>
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
        <link href=\"https://fonts.googleapis.com/css?family=Playfair+Display|Playfair+Display+SC\" rel=\"stylesheet\"/>


    </head>
    <body>
      <!-- Menu -->
      <nav class=\"navbar navbar-default\">
        <div class=\"container-fluid\">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
              <span class=\"sr-only\">Menu</span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_index");
        echo "\">Chocolate Messenger</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav navbar-right\">
              <li><a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_index");
        echo "\">Accueil</a></li>
              <li><a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_completed");
        echo "\">Completed</a></li>
              <li><a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_index");
        echo "\">Priorities</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

        ";
        // line 44
        $this->displayBlock('body', $context, $blocks);
        // line 45
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 46
        echo "        <!-- jQuery library -->
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>
        <!-- Latest compiled JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
        <!-- datetimepicker -->
        <script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>
        <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
        <script>
          \$( function() {
            \$( \"#datepicker\" ).datepicker();
          } );
        </script>
    </body>
</html>
";
        
        $__internal_546a44ee14b5ad94872ae91146aef7f2e66eb367f1b088506bbd3eacc8db5bc2->leave($__internal_546a44ee14b5ad94872ae91146aef7f2e66eb367f1b088506bbd3eacc8db5bc2_prof);

        
        $__internal_c8af7767a1ddbfd97a6229827c321608a0777422369bd00c4e7e668d69f6abfd->leave($__internal_c8af7767a1ddbfd97a6229827c321608a0777422369bd00c4e7e668d69f6abfd_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_165795f54b359084c4862bdbf113f7811e59935878031c75fec02cbea48e9744 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_165795f54b359084c4862bdbf113f7811e59935878031c75fec02cbea48e9744->enter($__internal_165795f54b359084c4862bdbf113f7811e59935878031c75fec02cbea48e9744_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1d0bc5d0ce08df156db18433c8c80b0c9d12fc7fca1b7e85508d17c7411d8410 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d0bc5d0ce08df156db18433c8c80b0c9d12fc7fca1b7e85508d17c7411d8410->enter($__internal_1d0bc5d0ce08df156db18433c8c80b0c9d12fc7fca1b7e85508d17c7411d8410_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Chocolate Messenger";
        
        $__internal_1d0bc5d0ce08df156db18433c8c80b0c9d12fc7fca1b7e85508d17c7411d8410->leave($__internal_1d0bc5d0ce08df156db18433c8c80b0c9d12fc7fca1b7e85508d17c7411d8410_prof);

        
        $__internal_165795f54b359084c4862bdbf113f7811e59935878031c75fec02cbea48e9744->leave($__internal_165795f54b359084c4862bdbf113f7811e59935878031c75fec02cbea48e9744_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_63cec0a91c7f3b97ea9e82689d4a5c6d53da4106595201c7fb68a29edde234a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63cec0a91c7f3b97ea9e82689d4a5c6d53da4106595201c7fb68a29edde234a6->enter($__internal_63cec0a91c7f3b97ea9e82689d4a5c6d53da4106595201c7fb68a29edde234a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_5b5699d3135018ef029be408e73db7a065004d334c77b6ea8b1c50908dfd2c8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b5699d3135018ef029be408e73db7a065004d334c77b6ea8b1c50908dfd2c8c->enter($__internal_5b5699d3135018ef029be408e73db7a065004d334c77b6ea8b1c50908dfd2c8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_5b5699d3135018ef029be408e73db7a065004d334c77b6ea8b1c50908dfd2c8c->leave($__internal_5b5699d3135018ef029be408e73db7a065004d334c77b6ea8b1c50908dfd2c8c_prof);

        
        $__internal_63cec0a91c7f3b97ea9e82689d4a5c6d53da4106595201c7fb68a29edde234a6->leave($__internal_63cec0a91c7f3b97ea9e82689d4a5c6d53da4106595201c7fb68a29edde234a6_prof);

    }

    // line 44
    public function block_body($context, array $blocks = array())
    {
        $__internal_45ced9c88d026c91311eb1798de5583313c0bab2afc03237af2cc8a01718d0f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45ced9c88d026c91311eb1798de5583313c0bab2afc03237af2cc8a01718d0f3->enter($__internal_45ced9c88d026c91311eb1798de5583313c0bab2afc03237af2cc8a01718d0f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_782b1a280cc8cb420f856c7b0b92d5f26638868037be77319de5ea2d3b0df83f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_782b1a280cc8cb420f856c7b0b92d5f26638868037be77319de5ea2d3b0df83f->enter($__internal_782b1a280cc8cb420f856c7b0b92d5f26638868037be77319de5ea2d3b0df83f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_782b1a280cc8cb420f856c7b0b92d5f26638868037be77319de5ea2d3b0df83f->leave($__internal_782b1a280cc8cb420f856c7b0b92d5f26638868037be77319de5ea2d3b0df83f_prof);

        
        $__internal_45ced9c88d026c91311eb1798de5583313c0bab2afc03237af2cc8a01718d0f3->leave($__internal_45ced9c88d026c91311eb1798de5583313c0bab2afc03237af2cc8a01718d0f3_prof);

    }

    // line 45
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_30abae48bd4ae2831a0ace81014203ebac75405791ea2165ab1a1ff9551c34e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30abae48bd4ae2831a0ace81014203ebac75405791ea2165ab1a1ff9551c34e0->enter($__internal_30abae48bd4ae2831a0ace81014203ebac75405791ea2165ab1a1ff9551c34e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_d1294b1e2e8a330f1b4e5a6cbfed76c049ae01b16e8ba3182e17dbed8bf02c9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1294b1e2e8a330f1b4e5a6cbfed76c049ae01b16e8ba3182e17dbed8bf02c9b->enter($__internal_d1294b1e2e8a330f1b4e5a6cbfed76c049ae01b16e8ba3182e17dbed8bf02c9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_d1294b1e2e8a330f1b4e5a6cbfed76c049ae01b16e8ba3182e17dbed8bf02c9b->leave($__internal_d1294b1e2e8a330f1b4e5a6cbfed76c049ae01b16e8ba3182e17dbed8bf02c9b_prof);

        
        $__internal_30abae48bd4ae2831a0ace81014203ebac75405791ea2165ab1a1ff9551c34e0->leave($__internal_30abae48bd4ae2831a0ace81014203ebac75405791ea2165ab1a1ff9551c34e0_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 45,  164 => 44,  147 => 8,  129 => 7,  105 => 46,  102 => 45,  100 => 44,  91 => 38,  87 => 37,  83 => 36,  74 => 30,  54 => 13,  48 => 10,  43 => 9,  41 => 8,  37 => 7,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>

        <title>{% block title %}Chocolate Messenger{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\">
        <!-- datetimepicker -->
        <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\"/>
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\">
        <link href=\"https://fonts.googleapis.com/css?family=Playfair+Display|Playfair+Display+SC\" rel=\"stylesheet\"/>


    </head>
    <body>
      <!-- Menu -->
      <nav class=\"navbar navbar-default\">
        <div class=\"container-fluid\">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
              <span class=\"sr-only\">Menu</span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"{{ path('message_index') }}\">Chocolate Messenger</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav navbar-right\">
              <li><a href=\"{{ path('message_index') }}\">Accueil</a></li>
              <li><a href=\"{{ path('message_completed') }}\">Completed</a></li>
              <li><a href=\"{{ path('author_index') }}\">Priorities</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
        <!-- jQuery library -->
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>
        <!-- Latest compiled JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
        <!-- datetimepicker -->
        <script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>
        <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
        <script>
          \$( function() {
            \$( \"#datepicker\" ).datepicker();
          } );
        </script>
    </body>
</html>
", "base.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/base.html.twig");
    }
}
