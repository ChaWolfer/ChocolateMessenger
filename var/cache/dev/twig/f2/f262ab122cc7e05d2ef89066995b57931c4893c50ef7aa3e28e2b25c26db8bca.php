<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_fe527007acf5d27a058d7cc714e187ad085376dff826d0e7e109bf7efeaf0710 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_273b2e08be60c96e114fa3755d94f35b78256974f02546c593486cbf39f164f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_273b2e08be60c96e114fa3755d94f35b78256974f02546c593486cbf39f164f1->enter($__internal_273b2e08be60c96e114fa3755d94f35b78256974f02546c593486cbf39f164f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_b552e8423243bbf3509fadcfe09db70d955a0d79c4af61e05ddf7f22e8393f93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b552e8423243bbf3509fadcfe09db70d955a0d79c4af61e05ddf7f22e8393f93->enter($__internal_b552e8423243bbf3509fadcfe09db70d955a0d79c4af61e05ddf7f22e8393f93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_273b2e08be60c96e114fa3755d94f35b78256974f02546c593486cbf39f164f1->leave($__internal_273b2e08be60c96e114fa3755d94f35b78256974f02546c593486cbf39f164f1_prof);

        
        $__internal_b552e8423243bbf3509fadcfe09db70d955a0d79c4af61e05ddf7f22e8393f93->leave($__internal_b552e8423243bbf3509fadcfe09db70d955a0d79c4af61e05ddf7f22e8393f93_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
