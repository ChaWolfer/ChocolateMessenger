<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_1a4e254d99350e31e2f1320a6b3258f61bc6876a0452cf5602c2a2eef09494fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5606e516e323a1d021f21aa3729f1c97d00bf01ee6f7d75958727f6359d1d7df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5606e516e323a1d021f21aa3729f1c97d00bf01ee6f7d75958727f6359d1d7df->enter($__internal_5606e516e323a1d021f21aa3729f1c97d00bf01ee6f7d75958727f6359d1d7df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_192f0548a66e70cc4c5ceed27d98819877030014768fa3f206bf0bec00ac7314 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_192f0548a66e70cc4c5ceed27d98819877030014768fa3f206bf0bec00ac7314->enter($__internal_192f0548a66e70cc4c5ceed27d98819877030014768fa3f206bf0bec00ac7314_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_5606e516e323a1d021f21aa3729f1c97d00bf01ee6f7d75958727f6359d1d7df->leave($__internal_5606e516e323a1d021f21aa3729f1c97d00bf01ee6f7d75958727f6359d1d7df_prof);

        
        $__internal_192f0548a66e70cc4c5ceed27d98819877030014768fa3f206bf0bec00ac7314->leave($__internal_192f0548a66e70cc4c5ceed27d98819877030014768fa3f206bf0bec00ac7314_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.atom.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
