<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_5dd881581bc15836809338988f2ffb1d30def9304e678fa262239ebe510c712a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2846274f3c1625002e3469b33c37c8fa77fd0ccf59153978c992920a3293e44a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2846274f3c1625002e3469b33c37c8fa77fd0ccf59153978c992920a3293e44a->enter($__internal_2846274f3c1625002e3469b33c37c8fa77fd0ccf59153978c992920a3293e44a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_d2b6af9e5bf8730019d221afaffc23ec66d1e967c75644a0b1fc82403d15eb7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2b6af9e5bf8730019d221afaffc23ec66d1e967c75644a0b1fc82403d15eb7f->enter($__internal_d2b6af9e5bf8730019d221afaffc23ec66d1e967c75644a0b1fc82403d15eb7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_2846274f3c1625002e3469b33c37c8fa77fd0ccf59153978c992920a3293e44a->leave($__internal_2846274f3c1625002e3469b33c37c8fa77fd0ccf59153978c992920a3293e44a_prof);

        
        $__internal_d2b6af9e5bf8730019d221afaffc23ec66d1e967c75644a0b1fc82403d15eb7f->leave($__internal_d2b6af9e5bf8730019d221afaffc23ec66d1e967c75644a0b1fc82403d15eb7f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
