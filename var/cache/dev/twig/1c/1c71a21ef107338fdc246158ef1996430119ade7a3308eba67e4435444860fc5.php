<?php

/* :Message:new.html.twig */
class __TwigTemplate_dff281cc31b49d65585715a4c4111e00f5987cb48e305f7310915ad32d59e5aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Message:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_964ae2599d9b8b6330723bbfd41e431dcfb3a04f4328b45b52eae7a2b8268420 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_964ae2599d9b8b6330723bbfd41e431dcfb3a04f4328b45b52eae7a2b8268420->enter($__internal_964ae2599d9b8b6330723bbfd41e431dcfb3a04f4328b45b52eae7a2b8268420_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:new.html.twig"));

        $__internal_8b2e72c1f71e404d1c9db8fb516ec0d479aa1a20be46fecb1ffed7c374607dc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b2e72c1f71e404d1c9db8fb516ec0d479aa1a20be46fecb1ffed7c374607dc6->enter($__internal_8b2e72c1f71e404d1c9db8fb516ec0d479aa1a20be46fecb1ffed7c374607dc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Message:new.html.twig"));

        // line 2
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme(($context["form"] ?? $this->getContext($context, "form")), array(0 => "bootstrap_3_layout.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_964ae2599d9b8b6330723bbfd41e431dcfb3a04f4328b45b52eae7a2b8268420->leave($__internal_964ae2599d9b8b6330723bbfd41e431dcfb3a04f4328b45b52eae7a2b8268420_prof);

        
        $__internal_8b2e72c1f71e404d1c9db8fb516ec0d479aa1a20be46fecb1ffed7c374607dc6->leave($__internal_8b2e72c1f71e404d1c9db8fb516ec0d479aa1a20be46fecb1ffed7c374607dc6_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_47f42363e3d92c4279acb6a445e6349172e760eaf9521b61dbb4e6042c9a30a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47f42363e3d92c4279acb6a445e6349172e760eaf9521b61dbb4e6042c9a30a8->enter($__internal_47f42363e3d92c4279acb6a445e6349172e760eaf9521b61dbb4e6042c9a30a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5464c2ae68823ea20e586160cbe4e9d19feb8c1070da409baff9af2f1fbfb2e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5464c2ae68823ea20e586160cbe4e9d19feb8c1070da409baff9af2f1fbfb2e2->enter($__internal_5464c2ae68823ea20e586160cbe4e9d19feb8c1070da409baff9af2f1fbfb2e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

<!--Form add / modify -->
<div class=\"container\">
  <div class=\"col-xs-12\">
    <h1 class=\"title\">Create a message </h1>
    <div class=\"row btnedit\">
      ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
      <div class=\"col-md-10\">

        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
      </div>
      <div class=\"col-md-2\"/>
      </div>
      <div class=\"col-md-10\">
        <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Create\" />
      </div>
      ";
        // line 23
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
      <div class=\"col-md-2 icon\"/>
        <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message_index");
        echo "\"><img class=\"icon-return\"src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/return.png"), "html", null, true);
        echo "\" alt=\"Return\"/></a>
      </div>
    </div>
  </div>
</div>


";
        
        $__internal_5464c2ae68823ea20e586160cbe4e9d19feb8c1070da409baff9af2f1fbfb2e2->leave($__internal_5464c2ae68823ea20e586160cbe4e9d19feb8c1070da409baff9af2f1fbfb2e2_prof);

        
        $__internal_47f42363e3d92c4279acb6a445e6349172e760eaf9521b61dbb4e6042c9a30a8->leave($__internal_47f42363e3d92c4279acb6a445e6349172e760eaf9521b61dbb4e6042c9a30a8_prof);

    }

    public function getTemplateName()
    {
        return ":Message:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 25,  77 => 23,  67 => 16,  61 => 13,  52 => 6,  43 => 5,  33 => 1,  31 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% form_theme form 'bootstrap_3_layout.html.twig' %}


{% block body %}


<!--Form add / modify -->
<div class=\"container\">
  <div class=\"col-xs-12\">
    <h1 class=\"title\">Create a message </h1>
    <div class=\"row btnedit\">
      {{ form_start(form) }}
      <div class=\"col-md-10\">

        {{ form_widget(form) }}
      </div>
      <div class=\"col-md-2\"/>
      </div>
      <div class=\"col-md-10\">
        <input class=\"btn btn-secondary submit\" type=\"submit\" value=\"Create\" />
      </div>
      {{ form_end(form) }}
      <div class=\"col-md-2 icon\"/>
        <a href=\"{{ path('message_index') }}\"><img class=\"icon-return\"src=\"{{ asset('img/return.png') }}\" alt=\"Return\"/></a>
      </div>
    </div>
  </div>
</div>


{% endblock %}
", ":Message:new.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Message/new.html.twig");
    }
}
