<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_f5a4e4e47a61dbc596b8a5cf6c4251cf9809cbaa7fe0192c935bf3aa4bd7f2e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a6d30330e357ae84122938b12f4774585f8b4794492372b601eebf2297ed1ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a6d30330e357ae84122938b12f4774585f8b4794492372b601eebf2297ed1ac->enter($__internal_1a6d30330e357ae84122938b12f4774585f8b4794492372b601eebf2297ed1ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_3b42cde3128093b2a3e99f11bcbb5231772d95c03851b60586097554a9d9cfbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b42cde3128093b2a3e99f11bcbb5231772d95c03851b60586097554a9d9cfbe->enter($__internal_3b42cde3128093b2a3e99f11bcbb5231772d95c03851b60586097554a9d9cfbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a6d30330e357ae84122938b12f4774585f8b4794492372b601eebf2297ed1ac->leave($__internal_1a6d30330e357ae84122938b12f4774585f8b4794492372b601eebf2297ed1ac_prof);

        
        $__internal_3b42cde3128093b2a3e99f11bcbb5231772d95c03851b60586097554a9d9cfbe->leave($__internal_3b42cde3128093b2a3e99f11bcbb5231772d95c03851b60586097554a9d9cfbe_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5fc1dfb22f7359e3c613b8f6842a55211b99a4849615092b4ab0feb1ebd50c03 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fc1dfb22f7359e3c613b8f6842a55211b99a4849615092b4ab0feb1ebd50c03->enter($__internal_5fc1dfb22f7359e3c613b8f6842a55211b99a4849615092b4ab0feb1ebd50c03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_7beec27f37d44e3f2b880c27fac80c145778e722a90bb270e723fff6d0e4eef7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7beec27f37d44e3f2b880c27fac80c145778e722a90bb270e723fff6d0e4eef7->enter($__internal_7beec27f37d44e3f2b880c27fac80c145778e722a90bb270e723fff6d0e4eef7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_7beec27f37d44e3f2b880c27fac80c145778e722a90bb270e723fff6d0e4eef7->leave($__internal_7beec27f37d44e3f2b880c27fac80c145778e722a90bb270e723fff6d0e4eef7_prof);

        
        $__internal_5fc1dfb22f7359e3c613b8f6842a55211b99a4849615092b4ab0feb1ebd50c03->leave($__internal_5fc1dfb22f7359e3c613b8f6842a55211b99a4849615092b4ab0feb1ebd50c03_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_e85c49ba9ef091c736b2f98d8803559c95c881d7cbfe30d76ff9873bcd6f42ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e85c49ba9ef091c736b2f98d8803559c95c881d7cbfe30d76ff9873bcd6f42ca->enter($__internal_e85c49ba9ef091c736b2f98d8803559c95c881d7cbfe30d76ff9873bcd6f42ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_38ababdbe30b16d75844ae0c20d45386c475130e7dd8932327b35136bf812b70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38ababdbe30b16d75844ae0c20d45386c475130e7dd8932327b35136bf812b70->enter($__internal_38ababdbe30b16d75844ae0c20d45386c475130e7dd8932327b35136bf812b70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_38ababdbe30b16d75844ae0c20d45386c475130e7dd8932327b35136bf812b70->leave($__internal_38ababdbe30b16d75844ae0c20d45386c475130e7dd8932327b35136bf812b70_prof);

        
        $__internal_e85c49ba9ef091c736b2f98d8803559c95c881d7cbfe30d76ff9873bcd6f42ca->leave($__internal_e85c49ba9ef091c736b2f98d8803559c95c881d7cbfe30d76ff9873bcd6f42ca_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_43d3d7cf02e1ebdc15f259748722c070d1ca03d7dd224a049f80446b693b82ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43d3d7cf02e1ebdc15f259748722c070d1ca03d7dd224a049f80446b693b82ef->enter($__internal_43d3d7cf02e1ebdc15f259748722c070d1ca03d7dd224a049f80446b693b82ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0a18999fb1ed7e059364151c488c36d670cfdb660b5406d2fa715afdf58d6b40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a18999fb1ed7e059364151c488c36d670cfdb660b5406d2fa715afdf58d6b40->enter($__internal_0a18999fb1ed7e059364151c488c36d670cfdb660b5406d2fa715afdf58d6b40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_0a18999fb1ed7e059364151c488c36d670cfdb660b5406d2fa715afdf58d6b40->leave($__internal_0a18999fb1ed7e059364151c488c36d670cfdb660b5406d2fa715afdf58d6b40_prof);

        
        $__internal_43d3d7cf02e1ebdc15f259748722c070d1ca03d7dd224a049f80446b693b82ef->leave($__internal_43d3d7cf02e1ebdc15f259748722c070d1ca03d7dd224a049f80446b693b82ef_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
