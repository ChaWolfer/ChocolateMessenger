<?php

/* :Author:show.html.twig */
class __TwigTemplate_05cb99a360bd8639a61a38e8f9703cc22b231afae34e6a8c9b48b334641bb67b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":Author:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3fe3c92ce9bdda50c73f34f2e56762ded4dd421ca2c4cfcb26f48c7306fb0e84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fe3c92ce9bdda50c73f34f2e56762ded4dd421ca2c4cfcb26f48c7306fb0e84->enter($__internal_3fe3c92ce9bdda50c73f34f2e56762ded4dd421ca2c4cfcb26f48c7306fb0e84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:show.html.twig"));

        $__internal_38b819a4c3ee4ad2e3f708afca249079864ed317f8fd2df97484d918c0134168 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38b819a4c3ee4ad2e3f708afca249079864ed317f8fd2df97484d918c0134168->enter($__internal_38b819a4c3ee4ad2e3f708afca249079864ed317f8fd2df97484d918c0134168_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Author:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3fe3c92ce9bdda50c73f34f2e56762ded4dd421ca2c4cfcb26f48c7306fb0e84->leave($__internal_3fe3c92ce9bdda50c73f34f2e56762ded4dd421ca2c4cfcb26f48c7306fb0e84_prof);

        
        $__internal_38b819a4c3ee4ad2e3f708afca249079864ed317f8fd2df97484d918c0134168->leave($__internal_38b819a4c3ee4ad2e3f708afca249079864ed317f8fd2df97484d918c0134168_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_857c54fa67292ffbbd0bb42b1941858f702e4284ed6d04240630ed39f093344a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_857c54fa67292ffbbd0bb42b1941858f702e4284ed6d04240630ed39f093344a->enter($__internal_857c54fa67292ffbbd0bb42b1941858f702e4284ed6d04240630ed39f093344a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_29b9c6efe5718c3ad8853c8e6ef3b788ba0c257be70d036d190286cd2ac3a97e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29b9c6efe5718c3ad8853c8e6ef3b788ba0c257be70d036d190286cd2ac3a97e->enter($__internal_29b9c6efe5718c3ad8853c8e6ef3b788ba0c257be70d036d190286cd2ac3a97e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<h1 class=\"title\">Author \"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "FirstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "LastName", array()), "html", null, true);
        echo "\"</h1>


<div class=\"container\">
  <div class=\"row message\">
    <div class=\"col-xs-9\">
      <p>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "Photo", array()), "html", null, true);
        echo "</p>     
      <p>First Name : ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "FirstName", array()), "html", null, true);
        echo "</p>
      <p>Last Name : ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "LastName", array()), "html", null, true);
        echo "</p>
      <p>Gender : ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "Gender", array()), "html", null, true);
        echo "</p>
      <p>Mail : ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "Mail", array()), "html", null, true);
        echo "</p>


    </div>
    <div class=\"col-xs-3 icon\">
      <a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_index");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/return.png"), "html", null, true);
        echo "\" alt=\"Return\"/></a>
      <a  href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("author_edit", array("id" => $this->getAttribute(($context["author"] ?? $this->getContext($context, "author")), "id", array()))), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/modify.png"), "html", null, true);
        echo "\" alt=\"Edit\"/></a>
      ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "<input class=\"icon-delete\" type=\"image\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/remove.png"), "html", null, true);
        echo "\" alt=\"Delete\"/>";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "

    </div>
  </div>
</div>





";
        
        $__internal_29b9c6efe5718c3ad8853c8e6ef3b788ba0c257be70d036d190286cd2ac3a97e->leave($__internal_29b9c6efe5718c3ad8853c8e6ef3b788ba0c257be70d036d190286cd2ac3a97e_prof);

        
        $__internal_857c54fa67292ffbbd0bb42b1941858f702e4284ed6d04240630ed39f093344a->leave($__internal_857c54fa67292ffbbd0bb42b1941858f702e4284ed6d04240630ed39f093344a_prof);

    }

    public function getTemplateName()
    {
        return ":Author:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 22,  93 => 21,  87 => 20,  79 => 15,  75 => 14,  71 => 13,  67 => 12,  63 => 11,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

<h1 class=\"title\">Author \"{{ author.FirstName }} {{ author.LastName }}\"</h1>


<div class=\"container\">
  <div class=\"row message\">
    <div class=\"col-xs-9\">
      <p>{{ author.Photo }}</p>     
      <p>First Name : {{ author.FirstName }}</p>
      <p>Last Name : {{ author.LastName }}</p>
      <p>Gender : {{ author.Gender }}</p>
      <p>Mail : {{ author.Mail }}</p>


    </div>
    <div class=\"col-xs-3 icon\">
      <a href=\"{{ path('author_index') }}\"><img src=\"{{ asset('img/return.png') }}\" alt=\"Return\"/></a>
      <a  href=\"{{ path('author_edit', { 'id': author.id }) }}\"><img src=\"{{ asset('img/modify.png') }}\" alt=\"Edit\"/></a>
      {{ form_start(delete_form) }}<input class=\"icon-delete\" type=\"image\" src=\"{{ asset('img/remove.png') }}\" alt=\"Delete\"/>{{ form_end(delete_form) }}

    </div>
  </div>
</div>





{% endblock %}
", ":Author:show.html.twig", "/home/charlotte/Documents/ChocolateMessenger/app/Resources/views/Author/show.html.twig");
    }
}
