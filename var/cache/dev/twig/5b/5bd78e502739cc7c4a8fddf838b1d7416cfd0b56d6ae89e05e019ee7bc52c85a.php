<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_3c823171a7055ca7ea8177e5e8086b8cc54f06469ba2968fa83ee1d6eba22e92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3937d3ba73b65bd838b50d42c63e797dbdeab822f47611e545683561e4851d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3937d3ba73b65bd838b50d42c63e797dbdeab822f47611e545683561e4851d3->enter($__internal_a3937d3ba73b65bd838b50d42c63e797dbdeab822f47611e545683561e4851d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        $__internal_543f1d043115190073ac6785d8324b847b3307b6c425e75194543cfad96ced9e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_543f1d043115190073ac6785d8324b847b3307b6c425e75194543cfad96ced9e->enter($__internal_543f1d043115190073ac6785d8324b847b3307b6c425e75194543cfad96ced9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_a3937d3ba73b65bd838b50d42c63e797dbdeab822f47611e545683561e4851d3->leave($__internal_a3937d3ba73b65bd838b50d42c63e797dbdeab822f47611e545683561e4851d3_prof);

        
        $__internal_543f1d043115190073ac6785d8324b847b3307b6c425e75194543cfad96ced9e->leave($__internal_543f1d043115190073ac6785d8324b847b3307b6c425e75194543cfad96ced9e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
", "@Framework/Form/range_widget.html.php", "/home/charlotte/Documents/ChocolateMessenger/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/range_widget.html.php");
    }
}
